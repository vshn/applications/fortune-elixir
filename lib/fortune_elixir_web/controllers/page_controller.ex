# tag::router[]
defmodule FortuneElixirWeb.PageController do
  use FortuneElixirWeb, :controller

  def index(conn, _params) do
    number = :rand.uniform(1000)
    message = elem(System.cmd("fortune", []), 0)
    hostname = elem(System.cmd("hostname", []), 0)
    version = "1.2-elixir"
    fortune = %{"number" => number, "message" => message, "hostname" => hostname, "version" => version }
    accept = case get_req_header(conn, "accept") do
      [version] -> version
      _ -> "text/html"
    end
    case accept do
      "application/json" -> json(conn, fortune)
      "text/plain" -> text(conn, "Fortune #{version} cookie of the day ##{number}:\n\n#{message}")
      _ -> render(conn, "index.html", fortune: fortune)
    end
  end
end
# end::router[]
